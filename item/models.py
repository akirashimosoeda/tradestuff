from django.db import models

# Create your models here.
class Item(models.Model):
    name = models.CharField(max_length=30)
    codename = models.CharField(max_length=30)
    description = models.TextField()
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('created',)
